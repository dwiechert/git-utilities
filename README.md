# git-utilities

Utility command line programs to help with Git.

## Things to add

### gpull

Git pull command that:

* Checks out master
* Deletes old branch
* Runs "prune" to delete remote branches

### gcommit

Git commit command that:

* Commits all current files
* Takes a commit message
* Pushes to the remote branch

## Build

Need to add CI to build the system:

* On commits run `go install` for each command
* Deploy each command to BitBucket artifacts:
	* [BitBucket Documentation](https://confluence.atlassian.com/bitbucket/deploy-build-artifacts-to-bitbucket-downloads-872124574.html)
	
## External Links

* [go-git project](https://github.com/src-d/go-git)
* [go-git documentation](https://godoc.org/gopkg.in/src-d/go-git.v4)
* [Commit](https://github.com/src-d/go-git/blob/master/_examples/commit/main.go)
* [Checkout](https://github.com/src-d/go-git/blob/master/_examples/checkout/main.go)
* [Prune](https://github.com/src-d/go-git/issues/624)